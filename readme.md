## Instasaver
> This is just-for-fun project to save memes on instagram (hopefully only that).

### Using instaloader and streamlit python module
Combine instaloader as back-end and steamlit as front-end then, **boom**, you become a ~~a stalker~~ *ehem i mean*, **a memes dealer**.

Jokes aside, this is just a project to make a user friendly web interface for instaloader (kind of) and use it without a hassle (hopefully).

#### References
- [The one who trigger me to made this](https://instasave.egoist.sh/).
- [Instaloader documentations](https://instaloader.github.io/as-module.html).
- [Streamlit](https://www.streamlit.io/).
- [Download instagram stories](https://github.com/instaloader/instaloader/issues/561).
- [View public profile anonymous (more advance version)](https://insta-stories.ru/).
- [Streamlit multiselect nested in if](https://github.com/streamlit/streamlit/issues/826).
- [Streamlit download file](https://discuss.streamlit.io/t/how-to-download-file-in-streamlit/1806/23).
